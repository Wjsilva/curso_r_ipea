# Explicando os diretórios

* **aulas**: contém os slides com conceitos, exemplos em *chuncks* e material ilustrativo

* **codigos**: *scripts* apresentados nos slides, desenvolvidos em aula e respostas de exercícios

* **docs**: arquivos de referência e ementa do curso

* **arquivos_aluno**: diretório de uso pessoal