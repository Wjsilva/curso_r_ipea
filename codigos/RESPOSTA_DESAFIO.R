## Resposta do DESAFIO

excel_sheets("dados/atlas2013_dadosbrutos_pt.xlsx")
siglas <- read_excel("dados/atlas2013_dadosbrutos_pt.xlsx",
                     "Siglas")
base_municipio <- read_excel("dados/atlas2013_dadosbrutos_pt.xlsx",
                             "MUN 91-00-10")

base_municipio <- base_municipio %>%
  mutate(IND_POP_ADULT = T_FUND18M/100,
         IND_FLUX_JOVEM = (T_FREQ5A6 +
                             T_FUND11A13 +
                             T_FUND15A17 +
                             T_MED18A20)/400,
         IDHMEDUC = (IND_POP_ADULT * 
                       IND_FLUX_JOVEM * 
                       IND_FLUX_JOVEM)^(1/3)   )

## selecionando variavéis de intesse
base_municipio <- base_municipio %>%
  select(Codmun7,ANO,Município,IDHM_E,IDHMEDUC)

##abra a base  base_municipio e verifique se está igual ao valor pré calculado

# criando variáveis uf e regiao
base_municipio <-  base_municipio %>%
  mutate(UF = trunc(Codmun7/100000),
         REGIAO = trunc(Codmun7/1000000),
         BRASIL = "BRASIL")

## CRIANDO BASE UF
base_uf <-  base_municipio %>%
  group_by(UF) %>%
  summarise(media_uf = mean(IDHMEDUC,na.rm = T))

## CRIANDO BASE REGIAO
base_regiao <-  base_municipio %>%
  group_by(REGIAO) %>%
  summarise(media_regiao = mean(IDHMEDUC,na.rm = T))

## CRIANDO BASE BRASIL
base_brasil <-  base_municipio %>%
  summarise(media_brasil = mean(IDHMEDUC,na.rm = T),
            BRASIL = "BRASIL")

## JUNTANDO BASES

base_geral <- left_join(base_municipio,base_uf,"UF") %>%
  left_join(base_regiao,"REGIAO") %>%
  left_join(base_brasil,"BRASIL")